import React, { Component } from "react";
import AppContext from '../utils/context/AppContext'
import io from "socket.io-client"
import { Slider } from "@blueprintjs/core"
import { AppContextConsumer } from '../utils/context/AppContext'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import _fn from '../utils/fn/_fn'
import "@blueprintjs/core/lib/css/blueprint.css"
import './ECG.css'

/**
 * Fetches socket server URL from env
 */
const SOCKET_URI = process.env.REACT_APP_SERVER_URI;
const SHIFT = JSON.parse(localStorage.getItem('_shift'));
const MV = JSON.parse(localStorage.getItem('_mv'));
const OFFSET_Y = JSON.parse(localStorage.getItem('_offsetY'));


/**
 * App Component
 *
 * initiaites Socket connection and handle all cases like disconnected,
 * reconnected again so that user can send messages when he is back online
 *
 * handles Error scenarios if requests from Axios fails.
 *
 */

class ECG extends Component {

    socket = null

    constructor(props) {
        super(props)
        this.state = {    
            theme: {}, 
            messages: [],
            chartData: [],
            ecgData: [],
            shift: _fn.get('_shift'),
            mv: _fn.get('_mv'),
            offsetY: _fn.get('_offsetY')            
        }

        this.initialiseChart = this.initialiseChart.bind(this)

        this.chartOptions = this.chartOptions.bind(this)

        this.chartThemeDark = this.chartThemeDark.bind(this)

        this.onMessageRecieved = this.onMessageRecieved.bind(this)

        this.setShift = this.setShift.bind(this)

        this.setmV = this.setmV.bind(this)

        this.setOffsetY = this.setOffsetY.bind(this)

        this.checkForAlert = this.checkForAlert.bind(this)

        _fn.logger( { data: props, text: "[ [ ECG props ] ]" } )

    }
    
    initialiseChart() {

        let options = this.chartOptions()

        // window.dataArray = new Array()

        window.Highcharts = Highcharts

        Highcharts.theme = this.chartThemeDark()

        Highcharts.setOptions(Highcharts.theme)

        var chart = Highcharts.chart('container', options)

        window.chart = chart

        this.setState({
            theme: this.context.theme
        })

    }
    chartThemeDark() {
        let theme = {
            name: 'dark',
            colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
                '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
            chart: {
                backgroundColor: '#323233',
                style: {
                    fontFamily: '\'Unica One\', sans-serif'
                },
                plotBorderColor: '#606063'
            },
            title: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase',
                    fontSize: '20px'
                }
            },
            subtitle: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase'
                }
            },
            xAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                title: {
                    style: {
                        color: '#A0A0A3'
                    }
                }
            },
            yAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                tickWidth: 1,
                title: {
                    style: {
                        color: '#A0A0A3'
                    }
                }

            },
            tooltip: {
                backgroundColor: 'rgba(0, 0, 0, 0.85)',
                style: {
                    color: '#F0F0F0'
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        color: '#F0F0F3',
                        style: {
                            fontSize: '13px'
                        }
                    },
                    marker: {
                        lineColor: '#333'
                    }
                },
                boxplot: {
                    fillColor: '#505053'
                },
                candlestick: {
                    lineColor: 'white'
                },
                errorbar: {
                    color: 'white'
                }
            },
            legend: {
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                itemStyle: {
                    color: '#E0E0E3'
                },
                itemHoverStyle: {
                    color: '#FFF'
                },
                itemHiddenStyle: {
                    color: '#606063'
                },
                title: {
                    style: {
                        color: '#C0C0C0'
                    }
                }
            },
            credits: {
                style: {
                    color: '#666'
                }
            },
            labels: {
                style: {
                    color: '#707073'
                }
            },
            drilldown: {
                activeAxisLabelStyle: {
                    color: '#F0F0F3'
                },
                activeDataLabelStyle: {
                    color: '#F0F0F3'
                }
            },
            navigation: {
                buttonOptions: {
                    symbolStroke: '#DDDDDD',
                    theme: {
                        fill: '#505053'
                    }
                }
            },
            // scroll charts
            rangeSelector: {
                buttonTheme: {
                    fill: '#505053',
                    stroke: '#000000',
                    style: {
                        color: '#CCC'
                    },
                    states: {
                        hover: {
                            fill: '#707073',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        },
                        select: {
                            fill: '#000003',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                },
                inputBoxBorderColor: '#505053',
                inputStyle: {
                    backgroundColor: '#333',
                    color: 'silver'
                },
                labelStyle: {
                    color: 'silver'
                }
            },
            navigator: {
                handles: {
                    backgroundColor: '#666',
                    borderColor: '#AAA'
                },
                outlineColor: '#CCC',
                maskFill: 'rgba(255,255,255,0.1)',
                series: {
                    color: '#7798BF',
                    lineColor: '#A6C7ED'
                },
                xAxis: {
                    gridLineColor: '#505053'
                }
            },
            scrollbar: {
                barBackgroundColor: '#808083',
                barBorderColor: '#808083',
                buttonArrowColor: '#CCC',
                buttonBackgroundColor: '#606063',
                buttonBorderColor: '#606063',
                rifleColor: '#FFF',
                trackBackgroundColor: '#404043',
                trackBorderColor: '#404043'
            }
        }

        return theme
    }
    checkForAlert(message) {
        let alertThresholds = [
            {
                type: 'excited',
                status: 'info',
                intent: 'primary',
                severity: 0,
                min: 8,
                max: 12
            },
            {
                type: 'disturbance',
                status: 'warning',
                intent: 'warning',
                severity: 1,
                min: 12,
                max: 16
            },
            {
                type: 'rush',
                status: 'warning',
                intent: 'warning',
                severity: 2,
                min: 16,
                max: 24
            },
            {
                type: 'cardiac arrest',
                status: 'danger',
                intent: 'danger',
                severity: 3,
                min: 24,
                max: 30
            },
            {
                type: 'danger',
                status: 'danger',
                severity: 4,
                min: 30,
                max: 40
            }
        ]

        for (let item of alertThresholds) {
            if (parseFloat(message.ecg) >= item.min && parseFloat(message) <= item.max) {
                this.context.createAlert(item)
            }
        }
    }
    chartOptions() {
        var options = {
            chart: {
                animation: false,
                type: 'spline',
                // width: '100%',
                // spacing: [0,0,0,0],
                margin: [5,5,5,5],
                // styledMode: false
            },
            boost: {
                enabled: true,
                useGPUTranslations: true
            },
            title: false,
            xAxis: {
                type: 'datetime',
                labels: {
                    overflow: 'justify'
                }
            },
            yAxis: {
                title: {
                    text: 'milliVolt'
                },
                max: this.state.mv,
                min: (this.state.mv * -1),
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{
                    from: 0,
                    to: 1,
                    color: 'rgba(68, 170, 213, 0.0)',
                    label: {
                        text: '',
                        style: {
                            borderBottom: '2px solid black',
                            height: '2px',
                            width: '100vw'
                            // color: '#606060'
                        }
                    }
                }, {
                    from: 8,
                    to: 12,
                    color: 'rgba(68, 170, 213, 0)',
                    label: {
                        text: 'Nominal',
                        style: {
                            color: '#606060'
                        }
                    }
                }, {
                    from: 12,
                    to: 16,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Excited',
                        style: {
                            color: '#606060',
                            borderBottom: '1px solid #000'
                        }
                    }
                }, {
                    from: 16,
                    to: 24,
                    color: 'rgba(68, 170, 213, 0)',
                    label: {
                        text: 'Cardiac Disturbance',
                        style: {
                            color: '#606060',
                            borderBottom: '1px solid #000'
                        }
                    }
                }, {
                    from: 24,
                    to: 30,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Adrenaline Rush',
                        style: {
                            color: '#606060',
                            borderBottom: '1px solid #000'
                        }
                    }
                }, {
                    from: 30,
                    to: 40,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Cardiac Arrest',
                        style: {
                            color: '#606060',
                            borderBottom: '1px solid #000'
                        }
                    }
                }]
            },
            tooltip: {
                valueSuffix: ' mV'
            },
            plotOptions: {
                spline: {
                    lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 6
                        }
                    },
                    marker: {
                        enabled: false
                    },
                    pointInterval: 60,
                    pointStart: 0
                }
            },
            series: [{
                name: "ECG",
                data: this.state.ecgData
            }],
            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        }
        return options
    }
    setShift(value) {

        this.setState({ shift: value })

        _fn.store({ label: '_shift', value: value })

        window.chart.redraw()

    }
    setmV(value) {

        this.setState({ mv: value })

        _fn.store({ label: '_mv', value: value })

        window.chart.yAxis[0].update({
            max: value,
            min: value * -1
        })

        window.chart.redraw()

    }
    setOffsetY(value) {

        this.setState({ offsetY: value })

        _fn.store({ label: '_offsetY', value: value })

        window.chart.redraw()

    }
    async createAlert(value) {

        let alertThresholds = [
            {
                type: 'excited',
                status: 'info',
                intent: 'primary',
                severity: 0,
                min: 8,
                max: 12
            },
            {
                type: 'disturbance',
                status: 'warning',
                intent: 'warning',
                severity: 1,
                min: 12,
                max: 16
            },
            {
                type: 'rush',
                status: 'warning',
                intent: 'warning',
                severity: 2,
                min: 16,
                max: 24
            },
            {
                type: 'cardiac arrest',
                status: 'danger',
                intent: 'danger',
                severity: 3,
                min: 24,
                max: 30
            },
            {
                type: 'danger',
                status: 'danger',
                severity: 4,
                min: 30,
                max: 40
            }
        ]

        // let alert = alerts[value]

        // return await _fn.createAlert(alert)

    }
    initSocketConnection() {
        this.socket = io.connect(SOCKET_URI);
    }

    setupSocketListeners() {
        this.socket.on("message", this.onMessageRecieved.bind(this))
    }

    /**
     *
     * @param {MessageRecievedFromSocket} message
     *
     * Triggered when message is received.
     * It can be a message from user himself but on different session (Tab).
     * so it decides which is the position of the message "right" or "left".
     *
     * increments unread count and appends in the messages array to maintain Chat History
     */

    onMessageRecieved(streamData) {

        console.log('[ message received ]', streamData )

        const series = window.chart.series[0]     
        

        for(let item of streamData) {

            const shift = series.data.length > this.state.shift

            const point = item.ecg

            series.addPoint(point, false, shift, true)
    
            window.chart.redraw()
    
            console.log('[ new data point ]', point )

        }

    }
    componentDidMount() {

        this.initSocketConnection()

        this.setupSocketListeners()

        this.initialiseChart()

    }    
    render() {
        return (
            <AppContext.Consumer>
                {props => (
                    <section id="App" className="">
                        <div className="chart-wrapper flex flex-column center w-100 vh100 overflow-auto relative ">
                            <div className="flex flex-row items-start f5 fw5 black-70 justify-start relative w-100">
                                <div
                                    id="container"
                                    style={{
                                        minWidth: '100vw',
                                        minheight: '100%',
                                        height: 'calc(100vh - 100px)',
                                        margin: '0 auto'
                                    }} />
                            </div>
                            <div className="shift-slider flex flex-row items-start justify-start relative h-100 w-100 bg-white ph6">
                                <div className="ph6 pv5 flex flex-row items-start f5 fw5 black-70 justify-start relative h-100 w-100 -bg-black-90">
                                    <Slider
                                        min={10}
                                        max={5000}
                                        stepSize={50}
                                        labelStepSize={200}
                                        onChange={this.setShift}
                                        value={parseFloat(this.state.shift)}
                                        className={"white-20 pv3"}
                                    />
                                </div>
                            </div>
                            <div className="mv-slider flex flex-row items-start justify-start  h-100 w-100 bg-white">
                                <div className=" ph5 pv5 flex flex-row items-start f5 fw5 black-70 justify-start h-100 w-100 -bg-black-90">
                                    <Slider
                                        min={1}
                                        max={2000}
                                        stepSize={10}
                                        labelStepSize={10}
                                        onChange={this.setmV}
                                        value={parseInt(this.state.mv)}
                                        vertical={true}
                                        className={"white-20 pv3 bg-black-30- w-100 h-100"}
                                    />
                                </div>
                            </div>
                            <div className="offsetY-slider flex flex-row items-start justify-start  h-100 w-100 bg-white">
                                <div className=" ph5 pv5 flex flex-row items-start f5 fw5 black-70 justify-start h-100 w-100 -bg-black-90">
                                    <Slider
                                        min={-40}
                                        max={40}
                                        stepSize={2}
                                        labelStepSize={10}
                                        onChange={this.setOffsetY}
                                        value={parseInt(this.state.offsetY)}
                                        vertical={true}
                                        className={"white-20 pv3 bg-black-30- w-100 h-100"}
                                    />
                                </div>
                            </div>
                        </div>
                    </section>
                )}
            </AppContext.Consumer>
        )
    }

}

export default ECG;

ECG.contextType = AppContextConsumer;

