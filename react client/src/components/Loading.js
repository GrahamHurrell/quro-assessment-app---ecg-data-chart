import React from "react"
// import * as animationData from '../data/lottie-ecg.json'

const Loading = ({ theme }) => (
    <section id="Loading" className={(theme.base) + (" fixed top-0 w-100 vh-100")} >
        <div className="flex flex-column flex-auto vh-100 w-100 items-center justify-center">
            <div className="flex flex-column pa5">
                <lottie-player
                    src={"/lf30_editor_qDJH3u.json"} background="transparent" speed="1"
                    style={{ width: '300px', height: '300px' }} loop autoplay >
                </lottie-player>
          
            </div>
        </div>
    </section>
)

export default Loading
