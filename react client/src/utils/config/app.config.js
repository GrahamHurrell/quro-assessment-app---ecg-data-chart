module.exports = {
  siteRoot: 'https://ecg.quantacom.co',
  siteMeta: {
    title: 'ECG Chart',
    author: 'Christopher Hurrell <questappsza@gmail.com>',
    image: '/image.png',
    description: 'Concept app for displaying ECG data.',
    siteUrl: 'https://ecg.quantacom.co'
  },
  storageProvider: {
    name: "jsonBlob",
    initUrl: "https://jsonblob.com/api/jsonBlob",
    url: "https://jsonblob.com/api/jsonBlob/",
  },
  email: {
    systemEmail: "quantacomsoftware@gmail.com",
    fromName: "[ HomeChef ]"
  }
}
