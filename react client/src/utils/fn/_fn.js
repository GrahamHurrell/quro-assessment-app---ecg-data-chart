import appconfig from '../config/app.config.js';
import alertMessages from './alertMessages';

var success = new Set([])

var logPrimary = [
    'background: #38c172',
    , 'padding: 1rem 0.7rem'
    , 'border: none'
    , 'color: white'
    , 'display: block'
    , 'line-height: auto'
    , 'text-align: left'
    , 'font-weight: bold', 'font-size: 0.9rem'
    , 'border-radius: 2px'
    , 'width: 100%'
    , 'margin-top: 1rem'
    , 'margin-bottom: 1rem'
].join(';')

var logInfo = [
    'background: #f3f3f3'
    , 'padding: 1rem 0.7rem'
    , 'border: none'
    , 'color: grey'
    , 'display: block'
    , 'line-height: 1.5rem'
    , 'text-align: left'
    , 'font-weight: light', 'font-size: 0.8rem'
    , 'border-radius: 2px'
    , 'width: 100%'
    , 'margin-top: 1rem'
    , 'margin-bottom: 1rem'
].join(';')

let _fn = {}

window._fn = _fn

_fn.initialise = {}

_fn.logger = async (config) => {
    const { data, text } = config;
    console.log('%c ' + text + ' ', logPrimary);
    console.log('%c ' + JSON.stringify(data) + ' ', logInfo);
    console.log('%c data >', logInfo, data);
}

_fn.store = (data) => {

    const { label, value } = data

    localStorage.setItem(label, JSON.stringify(value))

}

_fn.get = (data) => {
    
    return JSON.parse(localStorage.getItem(data))

}

_fn.hydrateDataFromLocalstorage = async (data) => {

    const { self } = data

    self.setState({
        theme: _fn.getTheme(),
        alerts: _fn.getLocallyStoredAlerts(),
        ready: true
    })

}

_fn.getLocallyStoredAlerts = () => {

    return JSON.parse(localStorage.getItem('_alerts'))

}

_fn.getTheme = () => {

    return JSON.parse(localStorage.getItem('_theme'))

}

_fn.dummyScript = async (data) => {
    _fn.logger({ data: null, text: 'dummy localstorage item does not exist, so we are running this dummy script.' })
    localStorage.setItem('dummy', JSON.stringify({ "test": "dummy" }))
    success.add({})

}

_fn.initialise._alerts = async (data) => {

    const { self } = data;

    return new Promise(async resolve => {

        _fn.store({ label: '_alerts', value: [] })

        self.setState({ alerts: [] })

        success.add({})

        resolve({ alerts: [] })
                
    })

}

_fn.initialise._theme = async (data) => {

    const { self } = data

    _fn.logger({ data: null, text: 'Setting theme' })

    return new Promise(async resolve => {

        let theme = { base: 'dark' }

        _fn.store({ label: '_theme', value: theme })


        self.setState({ theme })

        success.add({})

        resolve({ theme: theme })        

    })

}

_fn.initialise._shift = async (data) => {

    const { self } = data;

    return new Promise(async resolve => {

        _fn.store({ label: '_shift', value: 200 })

        self.setState({ shift: 200 })

        success.add({})

        resolve({ shift: 200 })

    })

}

_fn.initialise._offsetY = async (data) => {

    const { self } = data;

    return new Promise(async resolve => {

        _fn.store({ label: '_offsetY', value: 0 })

        self.setState({ offsetY: 0 })
        
        success.add({})
        
        resolve({ offsetY: 0 })

    })



}

_fn.initialise._mv = async (data) => {
    const { self } = data;

    return new Promise(async resolve => {

        _fn.store({ label: '_mv', value: 25 })

        self.setState({ mv: 25 })

        success.add({})

        resolve({ mv: 25 })        

    })


}
_fn.initialise.Startup = async (data) => {

    const { self } = data;

    let values = [        
        {
            label: 'Stored Messages DB remote endpoint',
            check: localStorage.getItem('db_messages_endpoint_id'),
            run: 'createMessagesDB({ self })'
        },
        {
            label: 'Stored Alerts DB remote endpoint',
            check: localStorage.getItem('db_alerts_endpoint_id'),
            run: 'createAlertsDB({ self })'
        },
        {
            label: 'Stored Notifications DB remote endpoint',
            check: localStorage.getItem('db_notifications_endpoint_id'),
            run: 'createNotificationsDB({ self })'
        },
        {
            label: 'Stored Alerts',
            check: localStorage.getItem('_alerts'),
            run: 'initialise._alerts({ self })'
        },
        {
            label: 'Stored Theme',
            check: localStorage.getItem('_theme'),
            run: 'initialise._theme({ self })'
        },
        {
            label: 'Stored Data Shift Value',
            check: localStorage.getItem('_shift'),
            run: 'initialise._shift({ self })'
        },
        {
            label: 'Stored Data OffsetY Value',
            check: localStorage.getItem('_offsetY'),
            run: 'initialise._offsetY({ self })'
        },
        {
            label: 'Stored Data MV Value',
            check: localStorage.getItem('_mv'),
            run: 'initialise._mv({ self })'
        }
    ]

    return new Promise(async resolve => {

        resolve(await _fn.checkExists({ self, values }))

    })

}
_fn.checkExists = async (data) => {

    return new Promise(async resolve => {

        let { self, values } = data

        var checkInterval = setInterval(async () => {

            for (let item of values) {

                if (item.check === null) {

                    eval('_fn.' + item.run)

                    _fn.logger({ 
                        data: item, 
                        text: "[ " + item.label + " ] does not exist, attempting to create ..." })

                }
                if (typeof item.check === 'string') {

                    // _fn.logger({ data: item, text: "[ " + item.label + " ] does exist, moving on ..." })

                    success.add(item)

                }

            }

            if (success.size === values.length) {

                let res = {
                    success: [...new Set(success)],
                    ok: true
                }

                // _fn.logger({ data: res, text: "Sending data to the front ..." })

                clearInterval(checkInterval)

                resolve(JSON.stringify(res))

            }


        }, 2000)

    })

}
_fn.createMessagesDB = async (data) => {

    console.log('[[ _fn.createMessagesDB ]]', data)

    const { self } = data;

    const dbInitData = {
        "type": "message",
        "text": "First message to initialise the remote messages db.",
        "createdAt": new Date()
    }

    const config = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "access-control-expose-headers": 'X-jsonBlob'
        },
        body: JSON.stringify(dbInitData)
    }

    const url = appconfig.storageProvider.initUrl
    

    await fetch(url, config).then(res => {


        _fn.store({ label: "db_messages_endpoint_id", value: res.headers.get('X-jsonBlob') })

        success.add(url)

        return res.headers.get('X-jsonBlob')

    })

}
_fn.createNotificationsDB = async (data) => {

    console.log('[[ _fn.createNotificationsDB ]]', data)

    const { self } = data;

    const dbInitData = {
        "type": "notification",
        "text": "First notification to initialise the remote notifications db.",
        "createdAt": new Date()
    }

    const config = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "access-control-expose-headers": 'X-jsonBlob'
        },
        body: JSON.stringify(dbInitData)
    }

    const url = appconfig.storageProvider.initUrl
    

    await fetch(url, config).then(res => {

        _fn.store({ label: "db_notifications_endpoint_id", value: res.headers.get('X-jsonBlob') })

        success.add(url)

        return res.headers.get('X-jsonBlob')

    })

}
_fn.createAlertsDB = async (data) => {

    console.log('[[ _fn.createAlertsDB ]]', data)

    const { self } = data;

    const dbInitData = {
        "type": "alert",
        "text": "First alert to initialise the remote alerts db.",
        "createdAt": new Date()
    }

    const config = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "access-control-expose-headers": 'X-jsonBlob'
        },
        body: JSON.stringify(dbInitData)
    }

    const url = appconfig.storageProvider.initUrl
    

    await fetch(url, config).then(res => {

        _fn.store({ label: "db_alerts_endpoint_id", value: res.headers.get('X-jsonBlob') })

        success.add(url)

        return res.headers.get('X-jsonBlob')

        

    })

}
_fn.getCollectionEndpointUrl = (type) => {

    const storageProviderUrl = appconfig.storageProvider.url;

    if (type === "alerts") {
        const id = JSON.parse(localStorage.getItem('db_alerts_endpoint_id'))
        const url = storageProviderUrl + id;
        return url
    }
    if (type === "messages") {
        const id = JSON.parse(localStorage.getItem('db_messages_endpoint_id'))
        const url = storageProviderUrl + id;
        return url
    }
    if (type === "notifications") {
        const id = JSON.parse(localStorage.getItem('db_notifications_endpoint_id'))
        const url = storageProviderUrl + id;
        return url
    }
}
_fn.createAlert = async (data) => {

    let { self, alert } = data

    alert.createdAt = new Date()

    alert.message = alertMessages.alert(alert)

    let alerts = await _fn.getLocallyStoredAlerts()

    alerts.push(alert)

    self.setState({ alerts: alerts })

    _fn.store({ label: '_alerts', value: alerts })


    return new Promise(async resolve => {

        const url = _fn.getCollectionEndpointUrl('alerts')

        const config = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(alerts)
        }

        await fetch(url, config).then(res => {

            return res.json()

        }).then(res => {

            return res

        }).then(data => {

            resolve({ data, ok: true })

        })
    })

}

export default _fn