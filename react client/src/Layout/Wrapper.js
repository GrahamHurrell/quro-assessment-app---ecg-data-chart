import React from 'react';
import Navigation from './Navigation';

class Wrapper extends React.Component {
    constructor(props) {
        super(props)
        this.state = {};
    }
    componentDidMount() {

    }
    render() {
        return (
            <main id="Wrapper" className="db relative w-100 h-100">
                {/* <Navigation /> */}
                <section id="Children-Wrapper" className="flex flex-column flex-auto w-100 h-100">
                    {this.props.children}
                </section>
            </main>
        );
    }

};

export default Wrapper;
