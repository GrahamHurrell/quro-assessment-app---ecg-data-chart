import React from 'react';
import AppContext from '../utils/context/AppContext'

import { Icon, Popover, Menu, MenuItem, } from "@blueprintjs/core";

import './style.css';

class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {

        return (
            <AppContext.Consumer>

                {props => (

                    <section
                        id="Nav"
                        className="z-99 fixed top-0 right-0 pa4 flex items-center justify-center"
                    >
                        <nav className="bp3-navbar bp3-dark- br2 bg-black-20 hover-bg-black-40">

                            <div style={{ margin: "0 auto", width: "auto" }}>

                                <div className="bp3-navbar-group bp3-align-right">

                                    <Popover content={
                                        <>
                                            <Menu className="">

                                                <MenuItem icon="warning-sign" text="Set new alert" className="white-60 f4 fw5 exo">

                                                    <MenuItem
                                                        disabled
                                                        onClick={() => props.createAlert(0)} icon="small-plus" className="white-60 f4 fw5 exo" text="Excited" />

                                                    <MenuItem
                                                        disabled
                                                        onClick={() => props.createAlert(1)} icon="small-plus" className="white-60 f4 fw5 exo" text="Disturbance" />

                                                    <MenuItem
                                                        disabled
                                                        onClick={() => props.createAlert(2)} icon="small-plus" className="white-60 f4 fw5 exo" text="Rush" />

                                                    <MenuItem
                                                        disabled
                                                        onClick={() => props.createAlert(3)} icon="small-plus" className="white-60 f4 fw5 exo" text="Cardiac Arrest" />

                                                </MenuItem>

                                            </Menu>

                                            <Menu className="">

                                                <MenuItem icon="warning-sign" text="Change Theme" className="white-60 f4 fw5 exo">

                                                    <MenuItem
                                                        disabled
                                                        onClick={() => props.changeTheme({ base: 'light' })}
                                                        icon="flash"
                                                        className="white-60 f4 fw5 exo"
                                                        text="Light" />

                                                    <MenuItem
                                                        disabled
                                                        onClick={() => props.changeTheme({ base: 'dark' })}
                                                        icon="moon"
                                                        className="white-60 f4 fw5 exo"
                                                        text="Dark" />

                                                </MenuItem>

                                            </Menu>

                                        </>
                                    } position={'auto'}>

                                        <button className="bp3-button bp3-minimal bp3-icon-user=">
                                            <Icon icon="menu" iconSize={15} className="mr2 db relative" />
                                        </button>

                                    </Popover>

                                </div>

                            </div>

                        </nav>

                    </section>
                )}

            </AppContext.Consumer>
        )
    }
}

export default Navigation