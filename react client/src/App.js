import React from "react"
import { Router, Route, Switch } from "react-router-dom"
import history from "./utils/history"
import { AppContextProvider } from './utils/context/AppContext'
import _fn from './utils/fn/_fn'
import { Position, Toaster } from "@blueprintjs/core"
import { AppToaster } from "./components/AppToaster"
import Loading from "./components/Loading"
import Wrapper from "./Layout/Wrapper"
import ECG from './Pages/ECG'

class App extends React.Component {

  constructor(props) {

    super(props)

    this.state = {
      ready: false,
      theme: {},
      alerts: []
    }

    this.createAlert = this.createAlert.bind(this)

    this.triggerAlert = this.triggerAlert.bind(this)

    this.changeTheme = this.changeTheme.bind(this)

  }
  changeTheme(data) {

    this.setState({
      theme: data
    })

    _fn.store({ label: '_theme', value: data })

    _fn.logger({ data: data, text: 'Switching theme ... ' })

  }
  async triggerAlert(data) {

    _fn.logger({ data: data, text: " Alert : [ " + data.message + " ]" })

    let alert = {
      message: data.message,
      intent: data.intent,
      icon: 'warning-sign',
      timeout: 5000
    }

    AppToaster.show(alert);

  }
  async createAlert(data) {

    await _fn.createAlert({ alert: data, self: this }).then(res => {

      return res

    }).then(res => {

      return res

    }).then(res_ => {

      this.triggerAlert(data)

    })

  }
  async componentDidMount() {

    await _fn.initialise.Startup({ self: this }).then(res => {

      this.setState({
        theme: _fn.getTheme(),
        alerts: _fn.getLocallyStoredAlerts(),
        ready: true
      })

    })

  }
  render() {
    if (!this.state.ready) {
      return <Loading theme={this.state.theme} />
    }
    return (

      <Router history={history} >
        <div
          id="app"
          className={(this.state.theme.base) + (" flex flex-column h-100 ")} >
          <AppContextProvider
            value={{
              changeTheme: this.changeTheme,
              theme: this.state.theme,
              alerts: this.state.alerts,
              triggerAlert: this.triggerAlert,
              createAlert: this.createAlert
            }}>
            <Wrapper >
              <Switch>
                <Route exact path="/" component={ECG} />
              </Switch>
            </Wrapper>
          </AppContextProvider>
        </div>
      </Router>
    )
  }
}

export default App
