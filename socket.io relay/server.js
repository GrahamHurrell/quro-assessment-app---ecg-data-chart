const express = require("express")
const app = express()
const port = 8002
var server = require("http").Server(app)
const io = require("socket.io")(server)
const cors = require("cors")
var mqtt = require('mqtt')

app.use(cors())

var mqttClient


io.on("connection", function(client) {

  mqttClient = mqtt.connect('mqtt://localhost')

  mqttClient.on('connect', data => {

    mqttClient.subscribe('ecg')  

  })

  mqttClient.on('message', (topic, message) => {

    let parsedMessage = JSON.parse(message)

    client.emit("message", parsedMessage)  

  })

})


server.listen(port, () =>
  console.log(`Example app listening on port ${port}!`)
)
