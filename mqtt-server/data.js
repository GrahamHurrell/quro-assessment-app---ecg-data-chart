const path = require('path');
const fs = require('fs');

//joining path of directory 
const directoryPath = path.join(__dirname, 'data');
var data = []


/* 
Async, read a file
*/
function filePathToString(filePath) {
    return new Promise(function(resolve){
        fs.readFile(filePath, function(err, buffer) {
            //handling error
            if (err) {
                console.log('Unable to read file: ' + err);
                throw err;
            }
            resolve(buffer.toString())
        })
    })
}

/* 
Long blocking, convert comma separated list to array of key value pairs
*/
function stringToDataArray(str) {
    // console.log('stringToDataArray',str)
    return str
        .trim()
        .split('\n')
        .join(',')
        .split(',')
        .reduce((accumulator, currentValue, currentIndex) => {
            if (currentIndex % 2) {
                accumulator[accumulator.length - 1].ecg = parseInt(currentValue)
                if (parseInt(currentValue) == NaN) {
                    console.log("str - " + str)
                }
            } else {
                accumulator.push({
                    timestamp: parseInt(currentValue),
                    ecg: null
                })
            }
            return accumulator
        }, [])
}

function sortBy(a, b) {
    if (a.timestamp > b.timestamp) {
        return 1;
    }
    if (b.timestamp > a.timestamp) {
        return -1;
    }
    return 0
}



module.exports = () => {
    return new Promise(function(resolve) {
        //passsing directoryPath and callback function
        fs.readdir(directoryPath, function (err, files) {
            //handling error
            if (err) {
                console.log('Unable to scan directory: ' + err);
                throw err;
            }

            //listing all files using forEach
            files
                .slice(1)
                .reduce((promise, fileName) => {
                    const filePath = path.join(directoryPath, fileName)
                    return promise
                        .then(() => filePathToString(filePath))
                        .then((str) => stringToDataArray(str))
                        .then((dataArr) => { data = data.concat(dataArr) })
                        .then(() => Promise.resolve(data))
                }, Promise.resolve(data))
                .then(() => {
                    resolve(data.sort(sortBy))
                })
        });
    })
}