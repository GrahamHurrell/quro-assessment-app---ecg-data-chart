var mosca = require('mosca');
var cors = require('cors');
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser');
var mqtt = require('mqtt');
var mqttClient = mqtt.connect('mqtt://localhost');
var mqttTopic = 'ecg';

var settings = {
    port: 1883
}

var timeOffset;
var lastPublishedIndex = -1;
var streamInterval;
var msFrequency = 20;

var moscaServer = new mosca.Server(settings);

const getData = require('./data')

/* 
Function that publishes simulated data to the MQTT broker every ≈20ms
*/
function startStreamSimulation() {
    getData().then(function(ecgReadings) {
        const timestampNow = Date.now()
        timeOffset = Date.now() - ecgReadings[0].timestamp
        const data = ecgReadings.map(value => ({timestamp: value.timestamp + timeOffset, ecg: value.ecg}))
    
        streamInterval = setInterval(function () {
            if (lastPublishedIndex >= (data.length - 1)) { 
                console.log("No more records")
                clearInterval(streamInterval);
            }

            const groupUpToTimestamp = Date.now() + msFrequency
            const group = []

            for (let i = lastPublishedIndex + 1; i < data.length; i++) {
                if (data[i].timestamp < groupUpToTimestamp) {
                    group.push(data[i])
                    lastPublishedIndex = i
                } else {
                   break 
                }
            }
    
            /* Publish data for current group to the corresponding MQTT topic as a JSON string  */
            if (group.length) {
                mqttClient.publish(mqttTopic, JSON.stringify(group));
            }
        }, msFrequency);
    })
}

mqttClient.on('connect', () => {
    console.log(' ')
    console.log('[ [ Mqtt connected. ] ]')
    mqttClient.subscribe(mqttTopic); //subscribe
    startStreamSimulation(); //publish
})

mqttClient.on('offline', () => {
    console.log(' ')
    console.log('[ [ Mqtt offline ] ]')
    mqttClient.unsubscribe(mqttTopic);
    clearInterval(streamInterval);
})

moscaServer.on('clientConnected', function (client) {
    console.log(' ')
    console.log('[ [ Client connected ] ]', client.id);

})

moscaServer.on('ready', () => {
    console.log(' ')
    console.log('[ [ Mosca server is up and running ] ]');
});
